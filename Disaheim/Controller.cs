﻿namespace Disaheim;

public class Controller
{
    public readonly List<IValuable> Valuables;
    /*
    public readonly List<Book> Books;
    public readonly List<Amulet> Amulets;
    public readonly List<Course> Courses;
    */

    public Controller()
    {
        /*
        Books = new List<Book>();
        Amulets = new List<Amulet>();
        Courses = new List<Course>();
        */
        Valuables = new List<IValuable>();
    }

    /*
    public void AddToList(Book book)
    {
        Books.Add(book);
    }

    public void AddToList(Amulet amulet)
    {
        Amulets.Add(amulet);
    }

    public void AddToList(Course course)
    {
        Courses.Add(course);
    }
    */
    public void AddToList(IValuable valuable)
    {
        Valuables.Add(valuable);
    }
}
