﻿using System.Linq.Expressions;

namespace Disaheim;
public class ValuableRepository : IPersistable
{
    private readonly List<IValuable> _valuables;

    public ValuableRepository()
    {
        _valuables = new List<IValuable>();
    }

    public void AddValuable(IValuable valuable)
    {
        _valuables.Add(valuable);
    }

    public IValuable GetValuable(string id)
    {
        return _valuables.Find(x => x.ToString().Contains(id));
    }

    public double GetTotalValue()
    {
        double TotalValue = 0;

        foreach (IValuable valuable in _valuables)
        {
            TotalValue += valuable.GetValue();
        }
        return TotalValue;
    }

    public int Count()
    {
        return _valuables.Count();
    }

    public void Save(string filename = "ValuableRepository.txt")
    {
        using (StreamWriter writer = new StreamWriter(filename))
        {
            foreach (var valuable in _valuables)
            {
                string Line = "";
                if (valuable is Book book)
                {
                    Line = $"{book.GetType().Name};{book.ItemId};{book.Title};{book.Price}";
                }
                else if (valuable is Amulet amulet)
                {
                    Line = $"{amulet.GetType().Name};{amulet.ItemId};{amulet.Design};{amulet.Quality}";
                }
                else if (valuable is Course course)
                {
                    Line = $"{course.GetType().Name};{course.Name};{course.DurationInMinutes};{course.GetValue()}";
                }
                writer.WriteLine(Line);
            }
        }
    }
    public void Load(string filename = "ValuableRepository.txt")
    {
        using StreamReader reader = new StreamReader(filename);
        {
            string data;
            while ((data = reader.ReadLine())!= null)
            {
                string[] ValueData = data.Split(";");
                switch (ValueData[0])
                {
                    case "Book":
                        {
                            Book book = new Book(ValueData[1]);
                            if (ValueData.Length > 2)
                            {
                                book.Title = ValueData[2];
                            }
                            if (ValueData.Length > 3)
                            {
                                book.Price = double.Parse(ValueData[3]);

                            }
                            _valuables.Add(book);
                            break;
                        }

                    case "Amulet":
                        {
                            Amulet amulet = new Amulet(ValueData[1]);
                            if (ValueData.Length > 2)
                            {
                                amulet.Design = ValueData[2];
                            }
                            if (ValueData.Length > 3)
                            {
                                amulet.Quality = ValueData[3] switch
                                {
                                    "high" => Level.high,
                                    "low" => Level.low,
                                    _ => Level.medium
                                };
                            }
                            _valuables.Add(amulet);
                            break;
                        }

                    case "Course":
                        {
                            Course course = new Course(ValueData[1]);
                            if (ValueData.Length > 2)
                            {
                                course.DurationInMinutes = Convert.ToInt32(ValueData[2]);
                            }
                            _valuables.Add(course);
                            break;
                        }
                }
            }
        }
    }
}
