﻿namespace Disaheim;

public class Book : Merchandise
{
    public string Title;
    public double Price { get; set; }

    public Book(string inItemId)
    {
        ItemId = inItemId;
    }

    public Book(string inItemId, string inTitle) : this(inItemId)
    {
        Title = inTitle;
    }
    public Book(string inItemId, string inTitle, double inPrice) : this(inItemId, inTitle)
    {
        Price = inPrice;
    }

    public override double GetValue()
    {
        return Price;
    }

    public override string ToString()
    {
        return $"ItemId: {ItemId}, Title: {Title}, Price: {Price}";

    }
}