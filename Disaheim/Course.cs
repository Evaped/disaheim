﻿namespace Disaheim;

public class Course : IValuable
{
    public string Name;
    public int DurationInMinutes { get; set; }
    public static double CourseHourValue { get; set; } = 875.00;

    public Course(string name)
    {
        Name = name;
    }
    public Course(string name, int durationInMinutes) : this(name)
    {
        DurationInMinutes = durationInMinutes;
    }

    public double GetValue()
    {
        double price;
        var One = DurationInMinutes;
        var Two = 60;

        double Price()
        {
            var result = (One / Two);
            if (One % Two != 0)
            {
                result++;
            }
            price = result * CourseHourValue;
            return price;
        }
        return Price();
    }

    public override string ToString()
    {
        double price;
        var One = DurationInMinutes;
        var Two = 60;
        var result = (One / Two);
        if (One % Two != 0)
        {
            result++;
        }
        price = result * CourseHourValue;
        return $"Name: {Name}, Duration in Minutes: {DurationInMinutes}, Value: {price}";
    }
}