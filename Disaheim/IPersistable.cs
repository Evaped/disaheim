﻿using System.Xml.Linq;

namespace Disaheim;
public interface IPersistable
{
    public void Save(string filename);

    public void Load(string filename);

}
