﻿namespace Disaheim;

public class Amulet : Merchandise
{
    public string Design;
    public Level Quality;

    public Amulet(string inItemId)
    {
        ItemId = inItemId;
    }
    public static double LowQualityValue { get; set; } = 12.5;
    public static double MediumQualityValue { get; set; } = 20;
    public static double HighQualityValue { get; set; } = 27.5;

    public Amulet(string inItemId, Level inQuality) : this(inItemId)
    {
        Quality = inQuality;
    }
    public Amulet(string inItemId, Level inQuality, string inDesign) : this(inItemId, inQuality)
    {
        Design = inDesign;
    }

    public override double GetValue()
    {
        double Value = Quality switch
        {
            Level.medium => MediumQualityValue,
            Level.low => LowQualityValue,
            Level.high => HighQualityValue,
            _ => throw new ArgumentOutOfRangeException()
        };

        return Value;
    }

    public override string ToString()
    {
        return $"ItemId: {ItemId}, Quality: {Quality}, Design: {Design}";
    }
}


